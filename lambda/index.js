/* *
 * This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
 * Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
 * session persistence, api calls, and more.
 * */
const Alexa = require('ask-sdk-core');
const utils = require('./util.js');
const APL_launchDocument = require('./documents/launchDocument.json');
const APL_playMusic = require('./documents/playMusic.json');
const APL_listRecommendations = require('./documents/listRecommendations.json');

// Import DDB adapter
const AWS = require("aws-sdk");
const ddbAdapter = require('ask-sdk-dynamodb-persistence-adapter');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    async handle(handlerInput) {
        //set up our Settings api foundations
        const serviceClientFactory = handlerInput.serviceClientFactory;
        const deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
    
        // initialize some variables
        var userTimeZone, greeting;
    
        // wrap the API call in a try/catch block in case the call fails for
        // whatever reason.
        try {
            const upsServiceClient = serviceClientFactory.getUpsServiceClient();
            userTimeZone = await upsServiceClient.getSystemTimeZone(deviceId);
        } catch (error) {
            userTimeZone = "error";
            console.log('error', error.message);
        }
    
        // calculate our greeting
        if(userTimeZone === "error"){
            greeting = "Hello.";
        } else {
            // get the hour of the day or night in your customer's time zone
            var hour = utils.getHour(userTimeZone);
            if(0<=hour&&hour<=4){
                greeting = "Hi night-owl!"
            } else if (5<=hour&&hour<=11) {
                greeting = "Good morning!"
            } else if (12<=hour&&hour<=17) {
                greeting = "Good afternoon!"
            } else if (17<=hour&&hour<=23) {
                greeting = "Good evening!"
            } else {
                greeting = "Howdy partner!"   
            }
        }
        
        const speakOutput = `${greeting} Welcome to the demo for NU Spark: Explainable and Configurable Personalization.`;

        //====================================================================
        // Add a visual with Alexa Layouts
        //====================================================================

        // Check to make sure the device supports APL
        if (
          Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)[
            'Alexa.Presentation.APL'
          ]
        ) {
            
            const viewportProfile = Alexa.getViewportProfile(handlerInput.requestEnvelope);
            const backgroundKey = viewportProfile === 'TV-LANDSCAPE-XLARGE' ? "Media/lights_1920x1080.png" : "Media/lights_1280x800.png";
            
            // add a directive to render our simple template
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: APL_launchDocument,
                datasources: {
                   myData: {
                       Title: greeting,
                       Subtitle: "Wecome NU Spark Demo!"
                   }
                },
            });
        }
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const PlayMusicIntentHandler = {
    canHandle(handlerInput) {
      return (
        Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
        Alexa.getIntentName(handlerInput.requestEnvelope) === 'PlayMusicIntent'
      );
    },
    handle(handlerInput) {
        //Get the slot values
        var songName = handlerInput.requestEnvelope.request.intent.slots.songQuery.value;

        const speakOutput = `Playing ${songName}.`;
        

        //====================================================================
        // Add a visual with Alexa Layouts
        //====================================================================
        
        // Check to make sure the device supports APL
        if (
          Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)[
            'Alexa.Presentation.APL'
          ]
        ) {
          // add a directive to render our simple template
          handlerInput.responseBuilder.addDirective({
            type: 'Alexa.Presentation.APL.RenderDocument',
            document: APL_playMusic,
            datasources: {
                metadata: {
                    songName: songName,
                    artistName: "Adele",
                    albumName: "25"
                }
            },
          });
        }
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const ThumbDownButtonEventHandler = {
    canHandle(handlerInput){
        return (Alexa.getRequestType(handlerInput.requestEnvelope) === 'Alexa.Presentation.APL.UserEvent'
            && handlerInput.requestEnvelope.request.source.id === 'thumbDownButton') || 
            (Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
            Alexa.getIntentName(handlerInput.requestEnvelope) === 'DislikeIntent');
    },
    handle(handlerInput){
        
        var artistName = handlerInput.requestEnvelope.request.intent.slots.artistName.value;
        var speakOutput = "placeholder";
        
        if (artistName !== 'hello'){
            
            speakOutput = `Gotcha! You don't like ${artistName}. Finding some other receipts for you.`
            
            if (
              Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)[
                'Alexa.Presentation.APL'
              ]
            ) {

                const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
                
                const listItems = sessionAttributes.data.receipts.filter(x => x.primaryText.toLowerCase().indexOf(artistName) === -1).map(function(x){ x.imageSource = utils.getS3PreSignedUrl(x.imagePrefix); return x; });
                
                // add a directive to render our simple template
                handlerInput.responseBuilder.addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: APL_listRecommendations,
                    datasources: {
                        imageListData: {
                            "type": "object",
                            "objectId": "imageListSample",
                            "title": "New recommendataions",
                            "listItems": listItems
                        }
                    }
                });
            
            }
            
        } else {
            
            //Get the slot values
            var songName = "hello";
            var albumName = "Can't Slow Down"
            
            speakOutput = `Gotcha! You don't like Adele. Playing ${songName} by ${artistName}`;
            
            //====================================================================
            // Add a visual with Alexa Layouts
            //====================================================================
            
            // Check to make sure the device supports APL
            if (
              Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)[
                'Alexa.Presentation.APL'
              ]
            ) {
              // add a directive to render our simple template
              handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: APL_playMusic,
                datasources: {
                    metadata: {
                        songName: songName,
                        artistName: artistName,
                        albumName: albumName
                    }
                },
              });
            }
        
        }

        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
}

const ListRecommendationsIntentHandler = {
    canHandle(handlerInput) {
      return (
        Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
        Alexa.getIntentName(handlerInput.requestEnvelope) === 'ListRecommendationsIntent'
      );
    },
    handle(handlerInput) {
        //Get the slot values
        var query = handlerInput.requestEnvelope.request.intent.slots.query.value;
        var domain = handlerInput.requestEnvelope.request.intent.slots.domain.value;
        var content = query;
        
        if (typeof query !== 'undefined') {
            content = query;
        } else {
            content = domain;
        }

        const speakOutput = `Finding some ${content} for you.`;
        

        //====================================================================
        // Add a visual with Alexa Layouts
        //====================================================================
        
        // Check to make sure the device supports APL
        if (
          Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)['Alexa.Presentation.APL']
        ) {
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
            
            const listItems = sessionAttributes.data.receipts.map(function(x){ x.imageSource = utils.getS3PreSignedUrl(x.imagePrefix); return x; });
            
            // add a directive to render our simple template
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: APL_listRecommendations,
                datasources: {
                    imageListData: {
                        "type": "object",
                        "objectId": "imageListSample",
                        "title": content,
                        "listItems": listItems
                    }
                }
            });
        }
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const HelloWorldIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'HelloWorldIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'Hello World!';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say hello to me! How can I help?';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet 
 * */
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'Sorry, I don\'t know about that. Please try again.';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
/* *
 * SessionEndedRequest notifies that a session was ended. This handler will be triggered when a currently open 
 * session is closed for one of the following reasons: 1) The user says "exit" or "quit". 2) The user does not 
 * respond or says something that does not match an intent defined in your voice model. 3) An error occurs 
 * */
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`);
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
    }
};
/* *
 * The intent reflector is used for interaction model testing and debugging.
 * It will simply repeat the intent the user said. You can create custom handlers for your intents 
 * by defining them above, then also adding them to the request handler chain below 
 * */
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};
/**
 * Generic error handling to capture any syntax or routing errors. If you receive an error
 * stating the request handler chain is not found, you have not implemented a handler for
 * the intent being invoked or included it in the skill builder below 
 * */
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const speakOutput = 'Sorry, I had trouble doing what you asked. Please try again.';
        console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const LoadDataInterceptor = {
    async process(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        // get persistent attributes, using await to ensure the data has been returned before
        // continuing execution
        var persistent = await handlerInput.attributesManager.getPersistentAttributes();
        if(!persistent) persistent = {};
        
        sessionAttributes.visits = (persistent.hasOwnProperty('visits')) ? persistent.visits : 0;
        sessionAttributes.data = (persistent.hasOwnProperty('data')) ? persistent.data : {};

        //set the session attributes so they're available to your handlers
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
    }
};
// This request interceptor will log all incoming requests of this lambda
const LoggingRequestInterceptor = {
    process(handlerInput) {
        console.log('----- REQUEST -----');
        console.log(JSON.stringify(handlerInput.requestEnvelope, null, 2));
    }
};

// Response Interceptors run after all skill handlers complete, before the response is
// sent to the Alexa servers.
const SaveDataInterceptor = {
    async process(handlerInput) {
        const persistent = {};
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        
        persistent.visits = sessionAttributes.visits + 1;
        persistent.data = sessionAttributes.data;
        // set and then save the persistent attributes
        handlerInput.attributesManager.setPersistentAttributes(persistent);
        let waiter = await handlerInput.attributesManager.savePersistentAttributes();
    }
};
// This response interceptor will log all outgoing responses of this lambda
const LoggingResponseInterceptor = {
    process(handlerInput, response) {
        console.log('----- RESPONSE -----');
        console.log(JSON.stringify(response, null, 2));
    }
};

/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom 
 * */
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        PlayMusicIntentHandler,
        ThumbDownButtonEventHandler,
        ListRecommendationsIntentHandler,
        HelloWorldIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        FallbackIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler)
    .addRequestInterceptors(
        LoadDataInterceptor,
        LoggingRequestInterceptor
    )
    .addResponseInterceptors(
        SaveDataInterceptor,
        LoggingResponseInterceptor
    )
    .addErrorHandlers(
        ErrorHandler)
    .withPersistenceAdapter(
        new ddbAdapter.DynamoDbPersistenceAdapter({
            tableName: process.env.DYNAMODB_PERSISTENCE_TABLE_NAME,
            createTable: false,
            dynamoDBClient: new AWS.DynamoDB({apiVersion: 'latest', region: process.env.DYNAMODB_PERSISTENCE_REGION})
        })
    )
    .withCustomUserAgent('sample/hello-world/v1.2')
    .withApiClient(new Alexa.DefaultApiClient())
    .lambda();